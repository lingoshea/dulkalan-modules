#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#include "dulkalan.h"

MODULE_LICENSE      ( "GPL" );
MODULE_AUTHOR       ( "Anivice CoUntilicty" );
MODULE_DESCRIPTION  ( "DULKALAN Module" );
MODULE_VERSION      ( "0.0.1" );

static int  Major;           /* Major number assigned to our device driver */
static int  connection_count = 0; /* count how many connection opened */

static char recv_buff [MAX_MSG_LEN];
static char recv_msg [MAX_MSG_LEN];
static int recv_msg_length = 0;

static int device_open ( struct inode *, struct file * );
static int device_release ( struct inode *, struct file * );
static ssize_t device_read ( struct file *, char *, size_t, loff_t * );
static ssize_t device_write ( struct file *, const char *, size_t, loff_t * );

#define MIN(a, b) ( (a) < (b) ? (a) : (b) )

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

static int __init dulkalan_init ( void ) 
{
    // Create device here
    Major = register_chrdev ( 0, DEVICE_NAME, &fops );
    if  ( Major < 0 ) 
    {
        printk ( KERN_ALERT "dulkalan: Registering char device failed with %d\n", Major );
        return Major;
    }

    printk ( KERN_INFO "dulkalan: DULKALAN loaded. Major number assigned as %d\n", Major );
    /* use 'mknod /dev/dulkalan c 238 0' to create the device */
    return DULKALAN_SUCCESS;
}

static void __exit dulkalan_exit ( void ) 
{
    /*
 * Unregister the device
 */
    unregister_chrdev ( Major, DEVICE_NAME );
    printk ( KERN_INFO "dulkalan: DULKALAN unloaded. Have a nice day.\n" );
}

// register entry and exit point
module_init ( dulkalan_init );
module_exit ( dulkalan_exit );

/*
 * Called when a process tries to open the device file, like
 * "cat /dev/mycharfile"
 */
static int device_open ( struct inode *inode, struct file *file ) 
{
    if  ( connection_count )
    {
        printk ( KERN_INFO "dulkalan: Connection rejected, module is currently busy.\n");
        return -EBUSY;
    }

    connection_count++;

    printk ( KERN_INFO "dulkalan: Connection %d accepted.\n", connection_count );

    try_module_get ( THIS_MODULE );
    return DULKALAN_SUCCESS;
}
/*
 * Called when a process closes the device file.
 */
static int device_release ( struct inode *inode, struct file *file ) 
{
    printk ( KERN_INFO "dulkalan: Connection closed. Thank you for your using.\n" );

    connection_count--; /* We're now ready for our next caller */
    /*
 * Decrement the usage count, or else once you opened the file, you'll
 * never get get rid of the module.
 */
    module_put ( THIS_MODULE );
    return 0;
}

/*
 * Called when a process, which already opened the dev file, attempts to
 *  * read from it.
 */
static ssize_t device_read ( struct file *filp, /* see include/linux/fs.h */
                           char *buffer,      /* buffer to fill with data */
                           size_t length,     /* length of the buffer */
                           loff_t *offset ) 
{
    /*
 * Number of bytes actually written to the buffer
 */
    int bytes_read = 0;
    const char * ptr = recv_msg;
    int read_count = MIN ( length, MAX_MSG_LEN );

    read_count = MIN ( read_count, recv_msg_length );
    
    /*
 * Actually put the data into the buffer
 */
    printk ( KERN_INFO "dulkalan: %d length of reading", read_count );
    while  ( read_count ) 
    {
        /*
 * The buffer is in the user data segment, not the kernel
 * segment so "*" assignment won't work. We have to use
 * put_user which copies data from the kernel data segment to
 * the user data segment.
 */
        put_user ( * ( ptr++ ) , buffer++ );
        read_count--;
        bytes_read++;
    }
    /*
 * Most read functions return the number of bytes put into the buffer
 */
    recv_msg_length = 0;
    
    return bytes_read;
}


/*
 * Called when a process writes to dev file /dev/dulkalan
 */
static ssize_t
device_write ( struct file *filp, const char __user *buff, size_t len, loff_t *off ) 
{
    char commands[5][5] = { };
    int offset;
    
    printk ( KERN_INFO "dulkalan: Incoming message received. Message length = %ld\n", len );

    if ( ( len < 15 ) || raw_copy_from_user ( recv_buff, buff, MIN ( len, MAX_MSG_LEN ) ) != 0 )
    {
        return -EFAULT;
    }

    /* Message  */
    offset = sscanf ( recv_buff,
           "%s %s",
           /* SOF ID: %d MSG: */
           commands[0], /* SOF */
           commands[1] /* MSG: */
    );

    // if it is a message head
    if ( ! ( strcmp ( commands[0], "SOF"  )
        || strcmp ( commands[1], "MSG:"  ) ) )
    {
        memcpy ( recv_msg, recv_buff + offset + 1, len - offset - 1 );
        // printk ( KERN_INFO "dulkalan: Message received! ID = %d, MSG = %s (%ld)", id, msg [id], len);
        recv_msg_length = len - offset - 1;
        return len;
    }

    printk ( KERN_ALERT "dulkalan: !! Dulkalan Protocol ERROR !!\n" );
    return -EPROTO;
    // return -EINVAL;
}
