#ifndef __DULKALAN__H__
#define __DULLALAN__H__

#define DULKALAN_SUCCESS    0
#define DEVICE_NAME         "dulkalan"  /* Dev name as it appears in /proc/devices */
#define MAX_MSG_LEN         4096        /* Max length of the message from the device */

#endif // __DULKALAN__H__
